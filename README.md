# Custom link plugin menu



## Getting started

Install "custom-links" plugin to your Peertube installation
https://github.com/McFlat/peertube-plugin-custom-links

## Configure it

Use the "plugin_config.txt" to configure the menu, in the plugin parameters. Replace the links by yours and the icons by images direct links.

## Add CSS

Use the "plugin_config.css" to add style to the menu (remove bullets, shift the text at the right place, etc.) : Configuration -> Advanced -> CSS

## Authors and acknowledgment

Thanks to Arnaud Champollion, from Aix-Marseille academie.

## License

Licensed under [EUPL](https://gitlab.com/apps_education/peertube/plugin-transcription/-/blob/main/LICENSE)

